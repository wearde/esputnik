<?php
/**
 * This file is part of ESputnik API connector
 *
 * @package ESputnik
 * @license MIT
 * @author Dmytro Kulyk <lnkvisitor.ts@gmail.com>
 */

namespace ESputnik;

class ESException
{
    
    protected $message;
    protected $code;

    /**
     * ESException constructor.
     * @param string $message
     * @param int $code
     */
    public function __construct($message = "", $code = 0)
    {
        $this->message = $message;
        $this->code = $code;
    }
    
    public function getError(){
        return array(
            'error' => [
                'code' => $this->code,
                'msg'  => $this->message
            ]            
        );
    }

}