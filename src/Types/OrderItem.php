<?php
/**
 * ESputnik
 *
 * @author igormoskal
 * @see https://github.com/RexGalicie
 * @license MIT
 */

namespace ESputnik\Types;

use ESputnik\Object;
/**
 * Class OrderItem
 *
 * @property string $externalItemId
 * @property string $name
 * @property string $category
 * @property integer $quantity
 * @property float $cost
 * @property string $url
 * @property string $imageUrl
 * @property string $description
 *
 * @link https://esputnik.com.ua/api/ns1_orderItem.html
 */
class OrderItem extends Object
{

    /**
     * @var string
     */
    public $externalItemId;
    /**
     * @var string
     */
    public $name;
    /**
     * @var string
     */
    public $category;
    /**
     * @var integer
     */
    public $quantity;
    /**
     * @var float
     */
    public $cost;
    /**
     * @var string
     */
    public $url;
    /**
     * @var string
     */
    public $imageUrl;
    /**
     * @var string
     */
    public $description;


    public function getArray(){
        return (array) $this;
    }
}