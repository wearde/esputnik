<?php
/**
 * ESputnik
 *
 * @author igormoskal
 * @see https://github.com/RexGalicie
 * @license MIT
 */

namespace ESputnik\Types;

use ESputnik\Object;
/**
 * Class ParametrizedLocator
 *
 * @property int $contactId
 * @property string $email
 * @property string $jsonParam
 * @property string $locator
 * @property array $error
 *
 * @link https://esputnik.com.ua/api/ns0_parametrizedLocator.html
 */
class ParametrizedLocator extends Object
{
    /**
     * @var array
     */
    protected $error = [];
    /**
     * @var int
     */
    protected $contactId;

    /**
     * @var string
     */
    protected $email;

    /**
     * @var string
     */
    protected $jsonParam;

    /**
     * @var string
     */
    protected $locator;
    
    
}