<?php
/**
 * ESputnik
 *
 * @author igormoskal
 * @see https://github.com/RexGalicie
 * @license MIT
 */

namespace ESputnik\Types;
use ESputnik\Object;

/**
 * Class SmartSend
 *
 * @property array $error
 * @property array $recipients
 * @property int $campaignId
 * @property Contact $contact
 * @property string $dedupeOn
 * @property string[] $groups
 * @link https://esputnik.com.ua/api/el_ns0_messageParams.html
 */
class SmartSend extends Object
{
    /**
     * @var array
     */
    protected $error = [];

    /**
     * @var array
     */
    protected $recipients = [];

    /**
     * @var integer
     */
    protected $campaignId;

    /**
     * @var bool
     */
    protected $allowUnconfirmed = false;

    /**
     * @var bool
     */
    protected $email = true;

    /**
     * @var string
     */
    protected $fromName;

    /**
     * * @param (ParametrizedLocator)[] $locators
     */
    public function setRecipients(array $locators)
    {        
        $this->recipients = array_map(function ($locator) {
            if ($locator instanceof ParametrizedLocator) {
                return array(
                    
                );
            }
            return $locator;
        }, $locators);
    }
}