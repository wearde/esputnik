<?php
/**
 * obyvka.dev
 *
 * @author igormoskal
 * @see https://github.com/RexGalicie
 * @license MIT
 */

namespace ESputnik\Types;
use ESputnik\Object;

/**
 * Class SmsMessage
 * 
 * @package ESputnik\Types
 * @property integer $id
 * @property string $name
 * @property string $from
 * @property string $text
 * @property string[] $tags
 *
 * @link  https://esputnik.com.ua/api/el_ns0_smsMessage.html
 */
class SmsMessage extends Object
{
    /**
     * @var array
     */
    protected $error;
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $from;

    /**
     * @var string
     */
    protected $text;

    /**
     * @var string[]
     */
    protected $tags = array();

}